import java.util.Scanner;

public class MainApp {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner scan = new Scanner(System.in);
		System.out.println("Intredueix el nombre del qual vosl saber el nombre de xifras");
		int num=scan.nextInt();
		System.out.println(numeroXifres(num));
		
	}
	
	private static int numeroXifres(int x) {
		int xifras=0;
		while(x!=0) {
			if(x/10!=0) {
				xifras=xifras+1;
			} 
			
			x=x/10;
		}
		return xifras+1;
	}

}
