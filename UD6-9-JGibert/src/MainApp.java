import java.util.Scanner;

public class MainApp {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner scan = new Scanner(System.in);
		System.out.println("Cuants numeros aleatoris vols generar?");
		int totalNum=scan.nextInt();
		int array[]=crearArray(totalNum);
		mostrarArray(array);
	}

	public static int[] crearArray(int arrayNum) {
		int array[]= new int[arrayNum];
		for(int i=0;i<array.length;i++) {
			array[i]=numAleatori();
		}
		return array;
		
		
	}
	
	public static void mostrarArray(int[] array) {
		for(int i=0;i<array.length;i++) {
			System.out.println(array[i]);
		}
	}
	private static int numAleatori() {
		int num=(int) (Math.random()*(9-1+1)+1);
		return num;
	}
}
