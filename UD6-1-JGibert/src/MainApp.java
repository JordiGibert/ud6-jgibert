import javax.swing.JOptionPane;

public class MainApp {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		String figura=JOptionPane.showInputDialog("Introdueix el nom de la figura");
		Double area=0.0;
		switch(figura) {
			case "cuadrat":
				area=areaCuadrat();
				JOptionPane.showMessageDialog(null, "La area del cuadrat es: "+area);
				break;
			case "cercle":
				area=areaCercle();
				JOptionPane.showMessageDialog(null, "La area del cercle es: "+area);
				break;
			case "triangle":
				area=areaTriangle();
				JOptionPane.showMessageDialog(null, "La area del triangle es: "+area);
				break;
			default:
				JOptionPane.showMessageDialog(null, figura+" no es una figura.");
		}
		
	}
	
	private static Double areaCercle() {
		String radiS=JOptionPane.showInputDialog("Introdueix els cm que medeix del radi");
		Double radi=Double.parseDouble(radiS);
		Double resultat = (radi*radi)*Math.PI;
		return resultat;
		
	}
	
	private static Double areaTriangle() {
		String baseS=JOptionPane.showInputDialog("Introdueix els cm que medeix la base");
		Double base=Double.parseDouble(baseS);
		String alturaS=JOptionPane.showInputDialog("Introdueix els cm que medeix la altura");
		Double altura=Double.parseDouble(alturaS);
		Double resultat = (base*altura)/2;
		return resultat;
		
	}
	private static Double areaCuadrat() {
		String costatS=JOptionPane.showInputDialog("Introdueix els cm que medeix un costat");
		Double costat=Double.parseDouble(costatS);
		Double resultat = costat*costat;
		return resultat;
		
	}

}
