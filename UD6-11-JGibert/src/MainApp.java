import java.util.Random;
import java.util.Scanner;

public class MainApp {
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner scan = new Scanner(System.in);
		System.out.println("Cuants numeros aleatoris vols generar?");
		int totalNum=scan.nextInt();
		System.out.println("Quin es el valor minim que vols tindre?");
		int numMin=scan.nextInt();
		System.out.println("Quin es el valor maxim que vols tindre?");
		int numMax=scan.nextInt();
		int array[]=new int[totalNum];
		array=omplirArray(numMax, numMin, array);
		int array2[]=array;
		array2=omplirArray(numMax, numMin, array);
		int[] arrayMulti=multiplicarArray(array, array2);
		System.out.print("Array 1: ");
		mostrarArray(array);
		System.out.print("Array 2: ");
		mostrarArray(array2);
		System.out.print("Array multiplicada: ");
		mostrarArray(arrayMulti);
	}

	public static int[] omplirArray(int numMax, int numMin, int[] array) {
		for(int i=0;i<array.length;i++) {
			array[i]=numAleatori(numMax, numMin);
		}
		return array;
		
		
	}
	
	public static void mostrarArray(int[] array) {
		for(int i=0;i<array.length;i++) {
			System.out.print(array[i]+" ");
		}
		System.out.println();
	}
	private static int numAleatori(int numMax, int numMin) {
		Random random = new Random();
		int num=random.nextInt((numMax-numMin))+numMin;
		return num;
	}
	
	private static int[] multiplicarArray(int[] x, int[] y) {
		int[] array=new int[x.length];
		for(int i=0; i<x.length;i++) {
			array[i]=x[i]*y[i];
		}
		return array;
		
	}
	

}
