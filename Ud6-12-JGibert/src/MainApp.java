import java.util.Scanner;

public class MainApp {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner scan = new Scanner(System.in);
		System.out.println("Cuants numeros aleatoris vols generar?");
		int totalNum=scan.nextInt();
		int array[]=new int[totalNum];
		array=omplirArray(300, 1, array);
		System.out.println("Quin es el ultim numero que vols mostrar?");
		int numFinal=scan.nextInt();
		mostrarArrayNumFinal(array, numFinal);
	}

	public static int[] omplirArray(int numMax, int numMin, int[] array) {
		for(int i=0;i<array.length;i++) {
			array[i]=numAleatori(numMax, numMin);
		}
		return array;
		
		
	}
	
	public static void mostrarArray(int[] array) {
		for(int i=0;i<array.length;i++) {
			System.out.print(array[i]+" ");
		}
		System.out.println();
	}
	
	public static void mostrarArrayNumFinal(int[] array, int numFinal) {
		System.out.println("Nombres acabats en "+numFinal);
		for(int i=0;i<array.length;i++) {
			if(array[i]%10==numFinal) {
				System.out.print(array[i]+" ");
			}
		}
		System.out.println();
	}
	private static int numAleatori(int numMax, int numMin) {
		int num=0;
			num=(int) (Math.random()*(numMax-numMin+1)+numMin);
			return num;
	}
	
	

}
