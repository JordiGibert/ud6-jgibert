import java.util.Scanner;

public class MainApp {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		System.out.println("Introdueix el tipo de monedes");
		Scanner scan = new Scanner(System.in);
		String moneda=scan.next();
		System.out.println("Introdueix la quantitat de euros que vols convertir");
		Double euros=scan.nextDouble();
		switch(moneda) {
			case "libra":
				System.out.println(convertirLibra(euros)+"libras");
				break;
			case "dolar":
				System.out.println(convertirDolar(euros)+"dolars");
				break;
			case "yen":
				System.out.println(convertirYen(euros)+"yens");
				break;
			default:
				System.out.println(moneda+" no es una moneda soportada");	
				
		}

	}
	
	private static Double convertirLibra(Double euros) {
		Double libras=euros*0.86;
		return libras;
	}
	
	private static Double convertirDolar(Double euros) {
		Double dolar=euros*1.28611;
		return dolar;
	}
	
	private static Double convertirYen(Double euros) {
		Double yen=euros*129.852;
		return yen;
	}
}
