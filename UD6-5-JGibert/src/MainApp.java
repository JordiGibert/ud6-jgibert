import java.util.Scanner;

public class MainApp {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner scan = new Scanner(System.in);
		System.out.println("Intredueix el nombre que vols convertir en binari");
		int num=scan.nextInt();
		System.out.println(convertirBinari(num));
		
	}
	
	private static String convertirBinari(int x) {
		String binari="";
		while(x!=0) {
			if(x%2==1) {
				binari="1"+binari;
			} else {
				binari="0"+binari;
			}
			
			x=x/2;
		}
		return binari;
	}

}
