import java.util.Scanner;

public class MainApp {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner scan = new Scanner(System.in);
		System.out.println("Introdueix el nombre del qual vols saber si es prim: ");
		int num=scan.nextInt();
		if(esPrim(num)) {
			System.out.println(num+" es prim");
		} else {
			System.out.println(num+" no es prim");
		}
		
	}

	private static boolean esPrim(int num) {
		boolean prim=true;
		for(int i=2;i<num;i++) {
			if((num%i)==0)
				prim=false;
		}
		return prim;
	}
}
