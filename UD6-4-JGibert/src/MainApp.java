import java.util.Scanner;

public class MainApp {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
			System.out.println("Introdueix el nombre del cual vols calcular el factorial");
			Scanner scan = new Scanner(System.in);
			int num=scan.nextInt();
			System.out.println("El factorial de "+num+" es: "+calcularFactorial(num));
	}

	private static long calcularFactorial(int num) {
		long factorial=num;
		for(int i=num-1;i>=1;i--) {
			factorial=factorial*i;
		}
		return factorial;
		
	}
}
