import java.util.Scanner;

public class MainApp {
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner scan = new Scanner(System.in);
		System.out.println("Cuants numeros aleatoris vols generar?");
		int totalNum=scan.nextInt();
		System.out.println("Quin es el valor minim que vols tindre?");
		int numMin=scan.nextInt();
		System.out.println("Quin es el valor maxim que vols tindre?");
		int numMax=scan.nextInt();
		int array[]=crearArray(totalNum, numMax, numMin);
		mostrarArray(array);
	}

	public static int[] crearArray(int arrayNum, int numMax, int numMin) {
		int array[]= new int[arrayNum];
		for(int i=0;i<array.length;i++) {
			array[i]=numAleatori(numMax, numMin);
		}
		return array;
		
		
	}
	
	public static void mostrarArray(int[] array) {
		for(int i=0;i<array.length;i++) {
			System.out.println(array[i]);
		}
	}
	private static int numAleatori(int numMax, int numMin) {
		int num=0;
		do {
			num=(int) (Math.random()*(numMax-numMin+1)+numMin);
		}while(esPrim(num)==false);
			return num;
	}
	
	private static boolean esPrim(int num) {
		boolean prim=true;
		for(int i=2;i<num;i++) {
			if((num%i)==0)
				prim=false;
		}
		return prim;
	}
}
